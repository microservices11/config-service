package com.example.configservice.controller;

import com.example.configservice.settings.DbSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    @Autowired
    private DbSettings dbSettings;

    @Autowired
    private Environment env;

//    @Value("${my.greeting}")
//    private String GreetingMessage;


    @GetMapping("/envdet")
    public String getEnv(){
        return env.toString();
    }
}
